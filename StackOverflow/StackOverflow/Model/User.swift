//
//  User.swift
//  StackOverflow
//
//  Created by Jacques Questiaux on 2018/07/28.
//  Copyright © 2018 Global Kinetic. All rights reserved.
//

import Foundation

struct User: Decodable {
    
    var reputation: Int
    var user_id: Int
    var user_type: String
    var accept_rate: Int?
    var profile_image: String
    var display_name: String
    var link: String
}
