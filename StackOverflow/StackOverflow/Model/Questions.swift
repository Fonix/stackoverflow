//
//  Questions.swift
//  StackOverflow
//
//  Created by Jacques Questiaux on 2018/07/28.
//  Copyright © 2018 Global Kinetic. All rights reserved.
//

import Foundation

struct Questions: Decodable {
    
    // Properties to JSON mapping
    enum CodingKeys : String, CodingKey {
        case questions = "items"
    }
    
    var questions: [Question]
}

struct Question: Decodable {
    
    var tags: [String]
    var owner: User?
    var is_answered: Bool
    var view_count: Int
    var answer_count: Int
    var score: Int
    var last_activity_date: Date
    var creation_date: Date
    var question_id: Int
    var link: String
    var title: String
    var body_markdown: String
}
