//
//  BaseViewController.swift
//  StackOverflow
//
//  Created by Jacques Questiaux on 2018/07/28.
//  Copyright © 2018 Global Kinetic. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        UIApplication.shared.statusBarView?.backgroundColor = UIColor.primaryBlue
    }
}
