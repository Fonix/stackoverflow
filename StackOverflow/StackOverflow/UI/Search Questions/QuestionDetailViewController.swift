//
//  QuestionDetailViewController.swift
//  StackOverflow
//
//  Created by Jacques Questiaux on 2018/07/29.
//  Copyright © 2018 Global Kinetic. All rights reserved.
//

import UIKit

class QuestionDetailViewController: BaseViewController {

    //MARK: IBOutlets
    @IBOutlet var questionTitleLabel: UILabel!
    @IBOutlet var questionTextView: UITextView!
    @IBOutlet var tagsLabel: UILabel!
    @IBOutlet var profileImage: UIImageView!
    @IBOutlet var userNameLabel: UILabel!
    @IBOutlet var userReputationLabel: UILabel!
    @IBOutlet var askedDateLabel: UILabel!
    @IBOutlet var profileImageActivityIndicator: UIActivityIndicatorView!
    
    var question:Question!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //Customize navigation bar
        navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.barTintColor = UIColor.primaryBlue
        navigationController?.navigationBar.tintColor = UIColor.white
        
        downloadProfileImage()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "'asked' d MMMM yyyy 'at' HH:mm"
        
        questionTitleLabel.text = question.title.unescape()
        questionTextView.text = question.body_markdown.unescape()
        userNameLabel.text = question.owner?.display_name.unescape() ?? "Unknown user"
        userReputationLabel.text = "\(question.owner?.reputation ?? 0)"
        askedDateLabel.text = dateFormatter.string(from: question.creation_date)
        tagsLabel.text = question.tags.joined(separator: ", ")
        
        questionTextView.clipsToBounds = false //Allows the scroolbar to be outside of the view
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        questionTextView.setContentOffset(CGPoint.zero, animated: false) //Sometimes the textview is scrolled to the bottom when addding content, so force it to the top
    }

    func downloadProfileImage() {
        guard let owner = question.owner else {
            return
        }
        
        profileImageActivityIndicator.startAnimating()
        DispatchQueue.global().async { //Download image asynchronously
            do {
                let url = URL(string:owner.profile_image)!
                let imageData: Data = try Data.init(contentsOf: url)
                DispatchQueue.main.sync {
                    self.profileImage.image = UIImage(data:imageData)
                }
                defer {
                    DispatchQueue.main.sync {
                        self.profileImageActivityIndicator.stopAnimating()
                    }
                }
            } catch {
                DispatchQueue.main.sync {
                    self.profileImage.image = #imageLiteral(resourceName: "profile-placeholder")
                }
            }
        }
    }
}
