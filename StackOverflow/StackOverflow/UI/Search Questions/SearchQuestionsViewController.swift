//
//  SearchQuestionsViewController.swift
//  StackOverflow
//
//  Created by Jacques Questiaux on 2018/07/28.
//  Copyright © 2018 Global Kinetic. All rights reserved.
//

import UIKit

class SearchQuestionsViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {

    //MARK: IBOutlets
    @IBOutlet var tableView: UITableView!
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var emptyTableLabel: UILabel!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    //MARK: Variables
    let webservice = StackOverflowWebservice()
    var questions:[Question]! {
        didSet {
            emptyTableLabel.isHidden = questions != nil && questions.count > 0 //show or hide label depending if there are search results
            emptyTableLabel.text = searchBar.text != nil && searchBar.text!.count > 0 ? "No Results" : "Stack Overflow Search" //show "No Results" if a search term is present, otherwise "Stack Overflow Search"
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBar.tintColor = UIColor.white
        searchBar.backgroundColor = UIColor.primaryBlue
        
        tableView.contentInset = UIEdgeInsetsMake(0, 0, 10, 0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func removeFocusFromSearchBar() {
        _ = searchBar.resignFirstResponder()
        searchBar.setShowsCancelButton(false, animated: true)
    }

    //MARK: UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let detailViewController = UIStoryboard(name: "Search Questions", bundle: nil).instantiateViewController(withIdentifier: "QuestionDetail") as! QuestionDetailViewController
        
        detailViewController.question = questions[indexPath.row]
        
        show(detailViewController, sender: nil)
    }
    
    //MARK: UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return questions?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: QuestionCell.identifier) as! QuestionCell
        
        cell.question = questions[indexPath.row]
        
        return cell
    }
    
    //MARK: UISearchBarDelegate
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        removeFocusFromSearchBar()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        removeFocusFromSearchBar()
        
        emptyTableLabel.isHidden = true //Hide emptyTableLabel when showing the activity indicator so they dont overlap
        activityIndicator.startAnimating()
        webservice.getQuestions(tag: searchBar.text) { [weak self] (questionsResponse, error) in
            self?.activityIndicator.stopAnimating()
            
            if questionsResponse != nil {
                self?.questions = questionsResponse!.questions
                self?.tableView.reloadData()
            } else if error != nil {
                let alert = UIAlertController(title: "Error",
                                              message: "There was a problem processing your request\n\(error?.localizedDescription ?? "")", preferredStyle: .alert)
                
                let ok = UIAlertAction(title: "Ok", style: .cancel)
                alert.addAction(ok)
                
                self?.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    //MARK: UIScrollViewDelegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        removeFocusFromSearchBar()
    }
}

class QuestionCell: UITableViewCell {
    
    static let identifier = "QuestionCell"
    
    //MARK: IBOutlets
    @IBOutlet var checkmark: UIImageView!
    @IBOutlet var questionLabel: UILabel!
    @IBOutlet var userLabel: UILabel!
    @IBOutlet var votesLabel: UILabel!
    @IBOutlet var answersLabel: UILabel!
    @IBOutlet var viewsLabel: UILabel!
    
    var question:Question! {
        didSet {
            setup()
        }
    }
    
    func setup() {
        checkmark.isHidden = !question.is_answered
        questionLabel.text = question.title.unescape()
        userLabel.text = "asked by \(question.owner?.display_name.unescape() ?? "unknown user")"
        votesLabel.text = "\(question.score) votes"
        answersLabel.text = "\(question.answer_count) answers"
        viewsLabel.text = "\(question.view_count) views"
    }
}
