//
//  UIColor+hex.swift
//  StackOverflow
//
//  Created by Jacques Questiaux on 2018/07/28.
//  Copyright © 2018 Global Kinetic. All rights reserved.
//

import UIKit

extension UIColor {
    var hexString: String {
        var red:CGFloat = 0
        var green:CGFloat = 0
        var blue:CGFloat = 0
        var alpha:CGFloat = 0
        
        getRed(&red, green: &green, blue: &blue, alpha: &alpha)
        
        let rgb:Int = (Int)(red*255)<<16 | (Int)(green*255)<<8 | (Int)(blue*255)<<0
        
        return String(format:"#%06x", rgb)
    }
    
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    /**
     Initialize colour from HEX integer
     */
    convenience init(hex hexValue: Int) {
        self.init(red: (hexValue >> 16) & 0xFF, green: (hexValue >> 8) & 0xFF, blue: hexValue & 0xFF)
    }
    
    /**
     Initialize colour from HEX string
     */
    convenience init(hex: String) {
        
        var hexString = hex.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if hexString.hasPrefix("#") {
            hexString.removeFirst()
        }
        
        var rgbValue: UInt32 = 0
        Scanner(string: hexString).scanHexInt32(&rgbValue)
        
        self.init(hex: Int(rgbValue))
    }
}
