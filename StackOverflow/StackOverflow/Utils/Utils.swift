//
//  Utils.swift
//  StackOverflow
//
//  Created by Jacques Questiaux on 2018/07/28.
//  Copyright © 2018 Global Kinetic. All rights reserved.
//

import Foundation

class Utils {

    static func addParametersToURL(_ url:URL, parameters:[String:String]?, percentEscaped:Bool = true) -> URL {
        
        guard let parameters = parameters, !parameters.isEmpty else {
            return url
        }
        
        guard var urlcomp = URLComponents(url: url, resolvingAgainstBaseURL: false) else {
            assertionFailure()
            return url
        }
        
        urlcomp.queryItems = parameters.compactMap({ (key, value) -> URLQueryItem? in
            
            if percentEscaped {
                // Percent encode values
                guard let encodedKey = key.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) else {
                    return nil
                }
                
                guard let encodedValue = value.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) else {
                    return nil
                }
                
                return URLQueryItem(name: encodedKey, value: encodedValue)
            }
            
            return URLQueryItem(name: key, value: value)
        })
        
        guard urlcomp.url != nil else {
            assertionFailure()
            return url
        }
        
        return urlcomp.url!
    }
}
