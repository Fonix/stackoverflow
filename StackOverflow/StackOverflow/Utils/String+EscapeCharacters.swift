//
//  String+EscapeCharacters.swift
//  StackOverflow
//
//  Created by Jacques Questiaux on 2018/07/29.
//  Copyright © 2018 Global Kinetic. All rights reserved.
//

import Foundation

extension String
{
    typealias SimpleToFromRepalceList = [(fromSubString:String,toSubString:String)]
    func simpleReplace(_ mapList:SimpleToFromRepalceList ) -> String
    {
        var string = self
        
        for (fromStr, toStr) in mapList {
            let separatedList = string.components(separatedBy:fromStr)
            if separatedList.count > 1 {
                string = separatedList.joined(separator: toStr)
            }
        }
        
        return string
    }
    
    //Seems swift doesnt have a good way of dealing with this kind of escaping
    func unescape() -> String
    {
        let mapList : SimpleToFromRepalceList = [
            ("&amp;",  "&"),
            ("&quot;", "\""),
            ("&#x27;", "'"),
            ("&#39;",  "'"),
            ("&#x92;", "'"),
            ("&#x96;", "-"),
            ("&gt;",   ">"),
            ("&lt;",   "<"),
            ("&#237;", "í")]
        
        return self.simpleReplace(mapList)
    }
}
