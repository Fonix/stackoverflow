//
//  UIApplication+StatusBar.swift
//  StackOverflow
//
//  Created by Jacques Questiaux on 2018/07/28.
//  Copyright © 2018 Global Kinetic. All rights reserved.
//

import UIKit

extension UIApplication {
    
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
    
}
