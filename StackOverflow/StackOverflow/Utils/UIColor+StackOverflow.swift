//
//  UIColor+StackOverflow.swift
//  StackOverflow
//
//  Created by Jacques Questiaux on 2018/07/28.
//  Copyright © 2018 Global Kinetic. All rights reserved.
//

import UIKit

extension UIColor {
    
    class var primaryBlue: UIColor {
        return UIColor(hex: "#4078C4")
    }
}
