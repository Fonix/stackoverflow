//
//  WebserivceController.swift
//  StackOverflow
//
//  Created by Jacques Questiaux on 2018/07/28.
//  Copyright © 2018 Global Kinetic. All rights reserved.
//

import Foundation

enum HttpMethod : String {
    case GET
    //TODO: add more types if needed
}

class WebserviceBaseController {
    
    let session = URLSession(configuration: URLSessionConfiguration.default)
    let baseURLString = Bundle.main.object(forInfoDictionaryKey: "WebserviceBaseURL") as! String
    let apiVersion = Bundle.main.object(forInfoDictionaryKey: "ApiVersion") as! String
    let jsonDecoder = JSONDecoder()
    let jsonEncoder = JSONEncoder()
    let defaultCachePolicy: URLRequest.CachePolicy = .useProtocolCachePolicy
    
    var baseURL:URL!
    
    init () {
        
        baseURL = URL(string: "\(baseURLString)\(apiVersion)/")
        assert(baseURL != nil, "Base URL is nil, check that the server URL has been set properly in the config")
        
        jsonDecoder.dateDecodingStrategy = .secondsSince1970 //stackoverflow seems to use this strategy
    }
    
    func performGET<T: Decodable>(path: String, parameters: [String : String]?, responseType: T.Type, completionHandler: @escaping (Int, T?, Data?, Error?) -> Void) {
        
        var url = baseURL.appendingPathComponent(path)
        
        url = Utils.addParametersToURL(url, parameters:parameters)
        
        var urlRequest = URLRequest(url: url, cachePolicy:defaultCachePolicy, timeoutInterval: 30)
        urlRequest.httpMethod = HttpMethod.GET.rawValue
        
        perform(request: urlRequest, responseType: responseType, completionHandler: completionHandler)
    }
    
    func perform<T: Decodable>(request urlRequest: URLRequest, responseType: T.Type, completionHandler: @escaping (Int, T?, Data?, Error?) -> Void) {
        
        print("URL Request: \(urlRequest.debugDescription)")
        
        let task = session.dataTask(with: urlRequest) {[unowned self] (data, urlResponse, networkError) in
            
            // Determine the HTTP status code
            var httpStatusCode: Int = 0
            
            if let httpResponse = urlResponse as? HTTPURLResponse {
                httpStatusCode = httpResponse.statusCode
            }
            
            guard networkError == nil else {
                DispatchQueue.main.async {
                    completionHandler(httpStatusCode, nil, nil, networkError)
                }
                
                return
            }
            
            if let data = data {
                // Decode the JSON response
                
                var responseObject: T?
                var decodeError: Error?
                
                do {
                    responseObject = try self.jsonDecoder.decode(responseType, from: data)
                } catch {
                    decodeError = error
                }
                
                DispatchQueue.main.async {
                    completionHandler(httpStatusCode, responseObject, data, decodeError)
                }
            }
        }
        
        task.resume()
    }
}
