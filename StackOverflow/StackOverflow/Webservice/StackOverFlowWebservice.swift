//
//  StackOverFlowWebservice.swift
//  StackOverflow
//
//  Created by Jacques Questiaux on 2018/07/28.
//  Copyright © 2018 Global Kinetic. All rights reserved.
//

import Foundation

class StackOverflowWebservice: WebserviceBaseController {
    
    func getQuestions (tag:String?, completionHandler: @escaping (Questions?, Error?) -> Void) {
        
        guard let tag = tag else {
            completionHandler(Questions(questions: []), nil) //If no search string, just return empty response
            return
        }
        
        let parameters = [
            "order":"desc",
            "sort":"activity",
            "site":"stackoverflow",
            "filter":"!9YdnSJ*_T", //filter that will remove html markup from the question body
            "pagesize":"20",
            "tagged":tag
        ]
        
        performGET(path: "questions", parameters: parameters, responseType: Questions.self) { (statusCode, response, data, error) in
            completionHandler(response, error)
        }
    }
}
